package volleyball;

import java.awt.Color;

public class ParticleShower {
	double[] x, y;
	Color color;
	double[] vx, vy;
	boolean[] done;

	public ParticleShower(int size, Color c, double power, double dx, double dy, double spread, double px, double py) {
		x = new double[size];
		y = new double[size];
		vx = new double[size];
		done = new boolean[size];
		vy = new double[size];

		for (int i = 0; i < size; i++) {
			x[i] = px;
			y[i] = py;
			double sp = 2 * Math.PI * Math.pow(Math.random(), 1 / spread);
			double t = (Math.random() * sp - sp / 2 + Math.atan2(dy, dx));
			vy[i] = Math.random() * Math.sin(t) * power;
			vx[i] = Math.random() * Math.cos(t) * power;
		}

		this.color = c;
	}

	public void update(double g) {
		g /= 600;
		for (int i = 0; i < x.length; i++) {

			x[i] += vx[i];
			y[i] += vy[i];

			if (x[i] <= 0 || x[i] >= 1 || y[i] <= 0 || y[i] >= 1)
				done[i] = true;

			vy[i] += g;
			vx[i] *= .999;
			vy[i] *= .999;
		}
	}

	public boolean isDone() {
		for (int i = 0; i < x.length; i++)
			if (!done[i])
				return false;

		return true;
	}

}
