package volleyball;

import java.util.LinkedList;
import java.util.Set;

public abstract class Player {
	Slime slime;
	int score = 0;
	String name;

	LinkedList<Powerup> collected = new LinkedList<Powerup>();

	abstract void move(Set<Integer> keys, GamePanel gp, java.util.List<Powerup> p);

	public void applyFirst(GamePanel gp) {
		if (!collected.isEmpty()) {
			Powerup p = collected.poll();
			p.apply(slime, gp);
			slime.powerups.add(p);
		}
	}
}
