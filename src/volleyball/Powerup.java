package volleyball;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Powerup {
	public static BufferedImage[] images = new BufferedImage[8];
	public static final Color[] POWERUP_COLORS = { Color.MAGENTA, Color.WHITE, Color.BLUE, Color.RED, Color.GREEN,
			Color.ORANGE, Color.LIGHT_GRAY };

	int type;
	Color original;
	double change, x, y;

	long durration = 10000, startTime;
	public int radius = 20;

	public Color getColor() {
		return POWERUP_COLORS[type];
	}

	Powerup(int type, int width) {
		this(type, getRandomChange(type), width);
	}

	Powerup(int type, double change, int width) {
		this.type = type;
		this.change = change;

		this.y = 0;
		this.x = Math.random() * width;
	}

	public boolean isGood() {
		return change > 0;
	}

	/*
	 * change radius (0)
	 * invis (1),
	 * speed (2),
	 * agility (3),
	 * create vertical/horizontal walls (4,5),
	 * allow passage onto the other side of the field (6)
	 */
	public void apply(Slime s, GamePanel gp) {
		s.borderColor = (type == 1 ? (s == gp.player1.slime ? gp.player2 : gp.player1).slime.color
				: POWERUP_COLORS[type]);

		switch (type) {
		case 0:
			s.r += change;
			break;
		case 1:
			(s == gp.player1.slime ? gp.player2 : gp.player1).slime.color = new Color(0, 0, 0, 0);
			break;
		case 2:
			s.speed += change;
			break;
		case 3:
			s.agility += change;
			break;
		case 4:
			gp.netHeightTo = gp.getHeight() - 50;
			break;
		case 5:
			gp.hWallTo = s.side ? -gp.getWidth() / 2 : gp.getWidth() / 2;
			break;
		case 6:
			s.passage = true;
			break;
		}

		startTime = System.currentTimeMillis();
	}

	public double fractionDone() {
		return (System.currentTimeMillis() - startTime) / (double) durration;
	}

	public void undo(Slime s, GamePanel gp) {
		switch (type) {
		case 0:
			s.r -= change;
			break;
		case 1:
			(s == gp.player1.slime ? gp.player2 : gp.player1).slime.color = (s == gp.player1.slime ? gp.player2
					: gp.player1).slime.oColor;
			break;
		case 2:
			s.speed -= change;
			break;
		case 3:
			s.agility -= change;
			break;
		case 4:
			gp.netHeightTo = 50;
			break;
		case 5:
			gp.hWallTo = 0;
			break;
		case 6:
			s.passage = false;
			break;
		}

		s.borderColor = Color.black;
	}

	private static double getRandomChange(int type) {
		switch (type) {
		case 0:
			return (Math.random() * 100) + 10;
		case 1:
			return 0;
		case 2:
			return (Math.random() * 16) + 1;
		case 3:
			return (Math.random() / 10) + .05;
		case 4:
			return 0;
		}

		return 0;
	}

	public void draw(Graphics2D g, int x, int y) {
		g.setStroke(new BasicStroke(3));
		g.setColor(POWERUP_COLORS[type]);
		g.drawOval(x - radius, y - radius, radius * 2, radius * 2);
		g.setStroke(new BasicStroke(1));

	}
}
