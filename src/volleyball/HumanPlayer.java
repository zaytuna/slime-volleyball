package volleyball;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Set;

public class HumanPlayer extends Player {
	public static final int ARROW_KEYS = 0;
	public static final int WASD = 1;
	public static final int IJKL = 2;

	int up, down, left, right, tl, tr, apply;
	boolean applied = false;

	HumanPlayer(int preset) {
		slime = new Slime();
		setKeys(preset);
	}

	public void setKeys(int preset) {
		switch (preset) {
		case 0:
			up = KeyEvent.VK_UP;
			down = KeyEvent.VK_DOWN;
			left = KeyEvent.VK_LEFT;
			right = KeyEvent.VK_RIGHT;
			tl = KeyEvent.VK_PERIOD;
			tr = KeyEvent.VK_SLASH;
			apply = KeyEvent.VK_COMMA;
			break;
		case 1:
			up = KeyEvent.VK_W;
			down = KeyEvent.VK_S;
			left = KeyEvent.VK_A;
			right = KeyEvent.VK_D;
			tl = KeyEvent.VK_Q;
			tr = KeyEvent.VK_E;
			apply = KeyEvent.VK_X;
			break;
		case 2:
			up = KeyEvent.VK_I;
			down = KeyEvent.VK_K;
			left = KeyEvent.VK_J;
			right = KeyEvent.VK_L;
			tl = KeyEvent.VK_U;
			tr = KeyEvent.VK_O;
			apply = KeyEvent.VK_M;
		}

	}

	HumanPlayer(double x, double y, double r, double a, Color c, int preset) {
		slime = new Slime(x, y, r, a, c);
		setKeys(preset);
	}

	HumanPlayer(double x, double y, double r, double a, Color c, int u, int d, int l, int ri, int tl, int tr) {
		slime = new Slime(x, y, r, a, c);

		this.up = u;
		this.down = d;
		this.left = l;
		this.right = ri;
		this.tl = tl;
		this.tr = tr;
	}

	@Override
	void move(Set<Integer> keys, GamePanel gp, java.util.List<Powerup> p) {
		if (keys.contains(left))
			slime.left();
		if (keys.contains(right))
			slime.right();
		if (keys.contains(tr))
			slime.rotR();
		if (keys.contains(tl))
			slime.rotL();
		if (keys.contains(up))
			slime.jump();
		if (keys.contains(down))
			slime.vy -= 5;
		if (keys.contains(apply) && !applied) {
			applyFirst(gp);
			applied = true;
		}
		if (!keys.contains(apply))
			applied = false;
	}
}
