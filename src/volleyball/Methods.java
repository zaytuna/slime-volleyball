package volleyball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

class Methods {
	public static void centerFrame(int frameWidth, int frameHeight, Component c) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		c
				.setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth,
						frameHeight);
	}

	public static <T> void shuffle(T[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math
					.random() * stuff.length));
	}

	public static void shuffle(int[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math
					.random() * stuff.length));
	}

	public static <T> void swap(T[] stuff, int posA, int posB) {
		T inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static Color colorMeld(Color a, Color b, double ratio) {
		return new Color(
				(int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio));
	}

	public static Color colorMeld(Color a, Color b, double ratio, int alpha) {
		return new Color(
				(int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio),
				alpha);
	}

	public static Point mouse() {
		return MouseInfo.getPointerInfo().getLocation();
	}

	public static void swap(int[] stuff, int posA, int posB) {
		int inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static double pseudoRandom(int x) {
		x = (x << 13) ^ x;
		return (1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0d);
	}

	public static Color randomColor() {
		return new Color((int) (Math.random() * 255),
				(int) (Math.random() * 255), (int) (Math.random() * 255), 250);
	}

	public static Color randomColor(int seed, int alpha) {
		return new Color((int) Math.abs(pseudoRandom(seed) * 255), (int) Math
				.abs(pseudoRandom(seed + 52) * 255), (int) Math
				.abs(pseudoRandom(seed * 5) * 255), alpha < 0 ? (int) Math
				.abs(pseudoRandom(seed * 29 + 17) * 255) : alpha);
	}

	public static <T> T[] createArray(ArrayList<T> ls, T[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static Object[] createObjectArray(ArrayList<? extends Object> ls,
			Object[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static <T> ArrayList<T> getList(T[] from) {
		ArrayList<T> list = new ArrayList<T>();

		for (T t : from)
			list.add(t);

		return list;
	}

	public static double max(double[][] array) {
		double max = array[0][0];
		for (double[] d : array)
			for (double number : d)
				if (number > max)
					max = number;

		return max;
	}

	public static double getMax(Collection<? extends Number> nums) {
		double max = nums.iterator().next().doubleValue();
		for (Number n : nums)
			if (n.doubleValue() > max)
				max = n.doubleValue();
		return max;

	}

	public static int getMaxLength(Map<?, ?>... maps) {
		int max = 0;
		for (Map<?, ?> map : maps)
			if (map.keySet().size() > max)
				max = map.keySet().size();
		return max;
	}

	public static double sum(double[] array) {
		double sum = 0;
		for (double d : array)
			sum += d;
		return sum;
	}

	public static void drawCenteredText(Graphics g, Font f, String s, int x,
			int y, int w, int h) {
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(f);
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());

		// Center text horizontally and vertically
		int p = (w - textWidth) / 2 + x;
		int q = (h - textHeight) / 2 + fm.getAscent() + y;

		g.drawString(s, p, q); // Draw the string.
	}

	public static String getFileContents(File f) {
		try {
			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(new FileReader(f));
			while (true) {
				String temp = in.readLine();
				if (temp == null)
					break;
				sb.append(temp + "\n");
			}
			in.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * http://www.dreamincode.net/code/snippet516.htm
	 */
	public static void insertionSort(int[] list, int length) {
		int firstOutOfOrder, location, temp;

		for (firstOutOfOrder = 1; firstOutOfOrder < length; firstOutOfOrder++) {
			// Starts at second term, goes until the end of the array.
			if (list[firstOutOfOrder] < list[firstOutOfOrder - 1]) {
				// If the two are out of order, we move the element to its
				// rightful place.
				temp = list[firstOutOfOrder];
				location = firstOutOfOrder;

				do { // Keep moving down the array until we find exactly where
						// it's supposed to go.
					list[location] = list[location - 1];
					location--;
				} while (location > 0 && list[location - 1] > temp);

				list[location] = temp;
			}
		}
	}

	public static Color getColor(Color color, int i) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), i);
	}
}