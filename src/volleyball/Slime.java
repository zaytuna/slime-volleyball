package volleyball;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Slime {
	double x, y, r, speed = 0, vy = 0, vx;
	double rot, vr, agility = Math.PI / 22;
	boolean side = false;

	List<Powerup> powerups = new ArrayList<Powerup>();
	Queue<Powerup> stored = new LinkedList<Powerup>();

	Color color, oColor, borderColor;
	public boolean passage = false;

	Slime() {
		this.x = 120;
		this.y = 0;
		this.color = Methods.randomColor();
		this.oColor = color;
		this.r = 80;
		this.rot = 0;
	}

	Slime(double x, double y, double r, double a, Color c) {
		this.x = x;
		this.y = y;
		this.color = c;
		this.oColor = c;
		this.r = r;
		this.rot = a;
	}

	Slime(double r, double speed, double agility, Color c) {
		this.color = c;
		this.oColor = c;
		this.r = r;
		this.rot = 0;
		this.agility = agility;
		this.speed = speed;
	}

	public void jump() {
		if (GamePanel.isResting(this)) {
			y++;
			vy = 17;
		}
	}

	public void left() {
		vx = -speed;
	}

	public void right() {
		vx = speed;
	}

	public void rotR() {
		vr = agility;
	}

	public void rotL() {
		vr = -agility;
	}

	public Slime clone() {
		Slime slime = new Slime(x, y, r, rot, oColor);
		slime.speed = speed;
		slime.agility = agility;
		slime.side = side;
		return slime;
	}
}
