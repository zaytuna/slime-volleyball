package volleyball;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements KeyListener, Runnable, MouseListener {
	private static final long serialVersionUID = -6705372358977439109L;

	public static final Color GROUND_COLOR = Color.ORANGE;
	public static final Color BALL_COLOR = Color.CYAN;
	public static final Color BACKGROUND = Color.BLACK;
	public static final Color NET = Color.ORANGE;
	public static final Color FADE = Color.RED;
	public static final int BLUR = 5;

	public static double GRAVITY = .6;
	public int maxPowerups = 4;

	Set<Integer> down = new HashSet<Integer>();
	long lastTime = 0;
	Menu menu = new Menu(this);

	boolean side = false;// true is left | false is right
	boolean colided = false;
	Map<Slime, LinkedList<Slime>> myBlur = new HashMap<Slime, LinkedList<Slime>>();
	LinkedList<Point> ballBlur = new LinkedList<Point>();
	Player player1, player2;

	ArrayList<Powerup> powerups = new ArrayList<Powerup>();
	Queue<Powerup> toAdd = new LinkedList<Powerup>();
	Queue<Powerup> toRem = new LinkedList<Powerup>();

	ArrayList<ParticleShower> showers = new ArrayList<ParticleShower>();
	Queue<ParticleShower> showerAdd = new LinkedList<ParticleShower>();

	double ballX = 120, ballY = 200, ballR = 20, ballVX, ballVY;
	double pauseRoation = 0;

	PixelDrawer graphics = new PixelDrawer(300, 200, this);

	ReentrantLock lock = new ReentrantLock();
	String message = "Game Over";

	private int mode = 0; // 0 is menu, 1 is game, 2 is paused, 3 is display a
	private int netHeight = 50, hwall = 0;
	public int netHeightTo = 50, hWallTo = 0;

	boolean drawV = false;

	public int maxScore = 5;

	// message, 4 is display a message, and force to
	// menu afterwards

	GamePanel(int w, int h) {
		setLayout(new BorderLayout());
		setSize(w, h);

		add(menu, BorderLayout.CENTER);

		setPreferredSize(new Dimension(w, h));

		player1 = new HumanPlayer(1);
		player2 = new HumanPlayer(0);
		setGame();

		addKeyListener(this);
		addMouseListener(this);

		menu.setVisible(true);
		restartGame();
		requestFocus();

		reset();
		new Thread(this).start();

	}

	public void setPaused(boolean b) {
		if (b)
			mode = 2;
		else
			play();
	}

	public void setGame() {
		player1.slime.side = true;
		player2.slime.side = false;

		myBlur.clear();

		myBlur.put(player1.slime, new LinkedList<Slime>());
		myBlur.put(player2.slime, new LinkedList<Slime>());
	}

	public void restartGame() {
		mode = 0;
		add(menu, BorderLayout.CENTER);
		menu.setVisible(true);
	}

	public void play() {
		mode = 1;
		// remove(menu);
		menu.setVisible(false);
	}

	public void paint() {
		graphics.clear(0);

		if (mode == 1 || mode == 3 || mode == 2) {

			// for (int i = 0; i < player1.score; i++)
			// graphics.fillOval(i * 25, 35, 23, 23, player1.slime.color
			// .getRGB());
			// for (int i = 0; i < player2.score; i++)
			// graphics.fillOval(800 - 25 - i * 25, 35, 23, 23,
			// player2.slime.color.getRGB());
			//
			// if (myBlur.keySet().contains(player1.slime)
			// && myBlur.get(player1.slime).size() > 1)
			// for (Slime s : myBlur.get(player1.slime))
			// drawSlime(graphics, s);
			// if (myBlur.keySet().contains(player2.slime)
			// && myBlur.get(player2.slime).size() > 1)
			// for (Slime s : myBlur.get(player2.slime))
			// drawSlime(graphics, s);
			//
			// int counter = 0;
			// for (Point p : ballBlur) {
			// graphics.setColor(Methods
			// .colorMeld(FADE, BALL_COLOR, (counter + 1)
			// / (double) BLUR, (255 * counter++) / BLUR));
			//
			// graphics.fillOval(p.x - (int) ballR, 550 - p.y - (int) ballR,
			// (int) (2 * ballR), (int) (2 * ballR));
			// }
			//
			// lock.lock();
			//
			// for (Powerup p : powerups)
			// graphics.drawImage(p.image, (int) p.x, 600 - (int) p.y);

			for (ParticleShower p : showers)
				graphics.draw(p);

			// lock.unlock();
			//
			// drawSlime(graphics, player1.slime);
			// drawSlime(graphics, player2.slime);
			//
			// drawPowerupBars(graphics, player1.slime);
			// drawPowerupBars(graphics, player2.slime);
			//
			// graphics.fillOval((int) (ballX - ballR),
			// (int) ((550 - ballY) - ballR), (int) (ballR * 2),
			// (int) (ballR * 2), BALL_COLOR.getRGB());
		}

		graphics.update();
	}

	public void paintComponent(Graphics g) {
		if (mode == 0)
			super.paintComponent(g);
		else if (mode != 0) {
			Graphics2D g2d = (Graphics2D) g;

			g2d.setColor(BACKGROUND);
			g2d.fillRect(0, 0, getWidth(), getHeight());

			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			if (mode == 1 || mode == 3 || mode == 2) {
				g2d.setColor(GROUND_COLOR);
				g2d.fillRect(0, getHeight() - 50, getWidth(), 60);
				g2d.setColor(NET);
				g2d.fillRect(getWidth() / 2 - 5, getHeight() - 50 - netHeight, 10, netHeight);
				g2d.fillRect(Math.min(getWidth() / 2, getWidth() / 2 + hwall), getHeight() - 100, Math.abs(hwall), 10);

				if (myBlur.keySet().contains(player1.slime) && myBlur.get(player1.slime).size() > 1)
					for (int i = 0; i < myBlur.get(player1.slime).size(); i++)
						drawSlime(g2d, myBlur.get(player1.slime).get(i), false);
				if (myBlur.keySet().contains(player2.slime) && myBlur.get(player2.slime).size() > 1)
					for (int i = 0; i < myBlur.get(player2.slime).size(); i++)
						drawSlime(g2d, myBlur.get(player2.slime).get(i), false);

				for (int i = 0; i < player1.collected.size(); i++) {
					g.setColor(player1.collected.get(i).getColor());
					g.fillRect(20 + i * 40, getHeight() - 45, 35, 35);
				}

				for (int i = 0; i < player2.collected.size(); i++) {
					g.setColor(player2.collected.get(i).getColor());
					g.fillRect(getWidth() - 55 - i * 40, getHeight() - 45, 35, 35);
				}

				lock.lock();

				int counter = 0;

				for (Point p : ballBlur) {
					g2d.setColor(Methods.colorMeld(FADE, BALL_COLOR, (counter + 1) / (double) BLUR, (255 * counter++)
							/ BLUR));

					g2d.fillOval(p.x - (int) ballR, getHeight() - 50 - p.y - (int) ballR, (int) (2 * ballR),
							(int) (2 * ballR));
				}

				for (Powerup p : powerups)
					p.draw(g2d, (int) p.x, (int) p.y);

				lock.unlock();

				drawSlime(g2d, player1.slime, true);
				drawSlime(g2d, player2.slime, true);

				drawPowerupBars(g2d, player1.slime);
				drawPowerupBars(g2d, player2.slime);

				g2d.setColor(BALL_COLOR);

				g2d.fillOval((int) (ballX - ballR), (int) ((getHeight() - 50 - ballY) - ballR), (int) (ballR * 2),
						(int) (ballR * 2));

				if (drawV) {
					g2d.setColor(Color.white);
					g2d.setStroke(new BasicStroke(3));
					g2d.drawLine((int) ballX, (int) (getHeight() - 50 - ballY), (int) ballX + (int) (3 * ballVX),
							(int) (getHeight() - 50 - ballY) - (int) (3 * ballVY));
					g2d.drawLine((int) ballX + (int) (2 * ballVX - ballVY), (int) (getHeight() - 50 - ballY)
							- (int) (2 * ballVY + ballVX), (int) ballX + (int) (3 * ballVX),
							(int) (getHeight() - 50 - ballY) - (int) (3 * ballVY));
					g2d.drawLine((int) ballX + (int) (2 * ballVX + ballVY), (int) (getHeight() - 50 - ballY)
							- (int) (2 * ballVY - ballVX), (int) ballX + (int) (3 * ballVX),
							(int) (getHeight() - 50 - ballY) - (int) (3 * ballVY));
					g2d.setStroke(new BasicStroke(1));
				}

				g2d.setColor(player1.slime.color);
				for (int i = 0; i < player1.score; i++)
					g2d.fillOval(i * 25, 5, 23, 23);

				g2d.setColor(player2.slime.color);
				for (int i = 0; i < player2.score; i++)
					g2d.fillOval(getWidth() - 25 - i * 25, 5, 23, 23);

				g2d.drawImage(graphics.getImage(), 0, 0, getWidth(), getHeight(), this);

				if (mode == 3) {
					g2d.setColor(Color.GREEN);
					g2d.setFont(new Font("SERIF", Font.BOLD, 80));
					Methods.drawCenteredText(g2d, g2d.getFont(), message, 0, 0, getWidth(), getHeight());
				} else if (mode == 2) {
					// g2d.drawImage(graphics.getImage(), 0, 0, this);
					g2d.setFont(new Font("SERIF", Font.BOLD, 100));
					g2d.translate(getWidth() / 2, getHeight() / 2);
					g2d.rotate(pauseRoation);

					for (int i = 0; i < BLUR; i++) {
						g2d.setColor(Methods.colorMeld(Color.BLUE, FADE, (i / (double) BLUR), 255 - i * 255 / BLUR));
						g2d.rotate(-.2 * i);
						Methods.drawCenteredText(g2d, g2d.getFont(), "Paused", -getWidth() / 2, -getHeight() / 2,
								getWidth(), getHeight());
						g2d.rotate(.2 * i);
					}

					g2d.setFont(new Font("SERIF", Font.BOLD, 80));
					g2d.setColor(Color.WHITE);
					Methods.drawCenteredText(g2d, g2d.getFont(), "Paused", -getWidth() / 2, -getHeight() / 2,
							getWidth(), getHeight());

					g2d.rotate(-pauseRoation);
					g2d.translate(-400, -300);
				}
			}

			else if (mode == 4) {
				g2d.setFont(new Font("SERIF", Font.BOLD, 60));
				g2d.setColor(Color.WHITE);
				Methods.drawCenteredText(g2d, g2d.getFont(), message, 0, 0, getWidth(), getHeight());
			}
		}

	}

	private void drawPowerupBars(Graphics g2d, Slime slime) {
		int count = 0;
		for (Powerup p : slime.powerups) {
			// int h = (int) (slime.rot < Math.PI / 2 ? slime.r *
			// Math.sin(slime.rot)
			// : (slime.rot < 3 * Math.PI / 2 ? slime.r : -slime.r *
			// Math.sin(slime.rot)));

			g2d.setColor(Color.WHITE);
			// g2d.drawRect((int) (slime.x - slime.r), (int) (550 - slime.y + h
			// - 20 * count), (int) (2 * slime.r), 20);

			g2d.drawRect(slime.side ? 0 : getWidth() - 200, count * 24, 200, 20);

			g2d.setColor(Methods.getColor(Powerup.POWERUP_COLORS[p.type], 200));
			// g2d.fillRect((int) (slime.x - slime.r), (int) (550 - slime.y + h
			// - 20 * count), (int) (2 * slime.r * p
			// .fractionDone()), 20);

			g2d.fillRect(slime.side ? 0 : getWidth() - 200, count * 24, (int) (200 * p.fractionDone()), 20);

			count++;
		}
	}

	public void updateBlur(Slime... slimes) {
		for (Slime s : slimes) {
			myBlur.get(s).addLast(s.clone());
			if (myBlur.get(s).size() >= BLUR)
				myBlur.get(s).removeFirst();

			int index = 0;

			for (Slime slime : myBlur.get(s)) {
				if (slime == null || slime.color == null)
					continue;
				slime.color = Methods.colorMeld(slime.color, FADE, (1D / (double) BLUR), slime.color.getAlpha()
						/ ++index);
			}
		}
	}

	public void update(Player player) {
		Slime s = player.slime;
		if (s == null)
			return;

		if (s.y > 0)
			s.vy -= GRAVITY;

		if (isResting(s))
			s.vy = 0;

		s.vx *= .9;

		s.rot = validateAngle(s.rot);

		s.y = Math.max(s.vy + s.y, (int) (s.rot < Math.PI / 2 ? s.r * Math.sin(s.rot) : (s.rot < 3 * Math.PI / 2 ? s.r
				: -s.r * Math.sin(s.rot))));

		int dxl = (int) (s.rot > Math.PI ? s.r : Math.abs(s.r * Math.cos(s.rot)));
		int dxr = (int) (s.rot < Math.PI ? s.r : Math.abs(s.r * Math.cos(s.rot)));
		if (s.passage) {
			s.x = Math.min(Math.max(s.x + s.vx, dxr), getWidth() - dxl);
		} else {
			s.x = Math.min(Math.max(s.x + s.vx, s.side ? dxr : getWidth() / 2 + dxr), s.side ? getWidth() / 2 - dxl
					: getWidth() - dxl);
		}
		// s.x = s.x+s.vx;
		// if((Math.tan(s.rot)*ballX+s.y>ballY) && (validateAngle(s.rot+
		// validateAngle(Math.atan2(-(s.x-ballX),-(s.y-ballY)))) >
		// Math.PI))return;

		updateBlur(s);

		lock.lock();
		for (Powerup p : powerups) {
			if ((s.r + p.radius) * (s.r + p.radius) > (s.x - p.x) * (s.x - p.x) + (getHeight() - 50 - s.y - p.y)
					* (getHeight() - 50 - s.y - p.y)) {
				player.collected.add(p);
				toRem.offer(p);
			}
		}

		for (int i = 0; i < s.powerups.size(); i++) {
			if (s.powerups.get(i).fractionDone() >= 1) {
				s.powerups.get(i).undo(s, this);
				s.powerups.remove(i);
				i--;
			}

		}

		lock.unlock();

		System.out.print(colided + "\t"
				+ ((s.r + ballR) * (s.r + ballR) > (s.x - ballX) * (s.x - ballX) + (s.y - ballY) * (s.y - ballY))
				+ "\t");

		if ((s.r + ballR) * (s.r + ballR) > (s.x - ballX) * (s.x - ballX) + (s.y - ballY) * (s.y - ballY)) {
			if (!colided) {

				double bxc = this.ballX - ballVX + s.vx;
				double byc = this.ballY - ballVY + s.vy;

				double dx = -(s.x - bxc), dy = -(s.y - byc);

				double angle = Math.atan2(dy, dx), vi = Math.atan2(-ballVY, -ballVX);

				if (isAngleBetweenCounterClockwise(angle, s.rot, validateAngle(s.rot + Math.PI))) {
					double total = Math.sqrt(ballVX * ballVX + ballVY * ballVY + s.vx * s.vx + s.vy * s.vy);

					if (!isAngleBetweenCounterClockwise(Math.atan2(dy - ballVX, dx - ballVY), s.rot, s.rot + Math.PI)
					/*
					 * || (ballR) * (ballR) > (s.x - ballX) * (s.x - ballX) +
					 * (s.y - ballY) * (s.y - ballY)
					 */) {
						ballX = s.x + 1 * ballR * Math.sin(s.r);
						ballY = s.y - 1 * ballR * Math.cos(s.r);
						ballVX = total * ballR * Math.sin(s.r);
						ballVY = -total * ballR * Math.cos(s.r);
					} else {
						this.ballVX = Math.cos(angle - (vi - angle)) * total - 15 * s.vr
								* Math.sin(angle - (vi - angle));
						this.ballVY = Math.sin(angle - (vi - angle)) * total - 15 * s.vr
								* Math.cos(angle - (vi - angle));
						this.ballX = bxc;
						this.ballY = byc;
					}
				}
				colided = true;
			}
		} else
			colided = false;

		System.out.print(colided + "\n");
	}

	public void updateBall() {
		ballVY = Math.min(ballVY, 20);
		ballVX = Math.min(ballVX, 20);
		ballVY -= GRAVITY;
		ballVX *= .993;
		ballVY *= .993;

		ballBlur.addLast(new Point((int) ballX, (int) ballY));
		if (ballBlur.size() >= BLUR)
			ballBlur.removeFirst();

		if (ballX <= ballR || ballX >= getWidth() - ballR
				|| (Math.abs(ballX - getWidth() / 2) < ballR && ballY < 50 + netHeight))
			ballVX *= -1;

		if (ballY >= getHeight() - ballR - 50) {
			ballVY *= -1;
		}
		ballY = Math.max(Math.min(ballY, getHeight() - 50 - ballR), ballR);
		ballX = Math.max(Math.min(ballX, getWidth() - ballR), ballR);

		if (ballY - ballR <= 50
				&& ((ballX > getWidth() / 2 && (hwall + getWidth() / 2 > ballX)) || (ballX < getWidth() / 2 && (hwall
						+ getWidth() / 2 < ballX)))) {
			ballVY = Math.abs(ballVY);
			ballY = 50 + ballR;
		}

		if (ballY <= ballR) {
			ballVY *= -.999;
			if (ballX < getWidth() / 2)
				player2.score++;
			else
				player1.score++;

			mode = 3;
			message = (ballX < getWidth() / 2 ? player2.name : player1.name) + " scored";
		}

		double newBallX = Math.min(Math.max(ballVX + ballX, ballR), getWidth() - ballR), newBallY = Math.min(Math.max(
				ballVY + ballY, ballR), getWidth() - ballR);

		if (newBallX < getWidth() / 2 && ballX > getWidth() / 2 || newBallX > getWidth() / 2 && ballX < getWidth() / 2)
			for (int i = 0; i < maxPowerups; i++)
				if (Math.random() > .94) {
					lock.lock();
					Powerup p = new Powerup((int) (Math.random() * 7), getWidth());
					toAdd.offer(p);
					lock.unlock();
				}

		ballX = newBallX;
		ballY = newBallY;

		for (int i = 0; i < powerups.size(); i++) {
			Powerup p = powerups.get(i);

			if ((p.x - ballX) * (p.x - ballX) + (getHeight() - 50 - p.y - ballY) * (getHeight() - 50 - p.y - ballY) <= (ballR + p.radius)
					* (ballR + p.radius)) {
				showerAdd.offer(new ParticleShower(200, p.getColor(),
						Math.sqrt(ballVX * ballVX + ballVY * ballVY) / 500, ballVX, ballVY, 2, p.x / getWidth(), p.y
								/ getHeight()));
				toRem.offer(p);
			}
		}
	}

	public void reset() {
		ballVX = 0;
		ballVY = 0;
		ballX = ballX < getWidth() / 2 ? getWidth() - 120 : 120;
		ballY = 400;

		player1.slime.x = 120;
		player1.slime.rot = 0;
		player1.slime.vr = 0;
		player1.slime.y = 0;
		player1.slime.vy = 0;
		player1.slime.vx = 0;

		player2.slime.x = getWidth() - 120;
		player2.slime.rot = 0;
		player2.slime.vr = 0;
		player2.slime.y = 0;
		player2.slime.vy = 0;
		player2.slime.vx = 0;
	}

	public static boolean isResting(Slime s) {
		return s.y == (int) (s.rot < Math.PI / 2 ? s.r * Math.sin(s.rot) : (s.rot < 3 * Math.PI / 2 ? s.r : -s.r
				* Math.sin(s.rot)));
	}

	public static double validateAngle(double ang) {
		return (((ang % (2 * Math.PI)) + 4 * Math.PI) % (2 * Math.PI));
	}

	public static void drawSlime(PixelDrawer g, Slime s) {
		if (Math.abs(s.rot) <= .01)
			g.fillArc((int) (s.x - s.r), (int) ((550 - s.y) - s.r), (int) (2 * s.r), (int) (2 * s.r), 0, Math.PI,
					s.color.getRGB());
		else
			g.fillArc((int) s.x - (int) (s.r), 550 - (int) s.y - (int) (s.r), (int) (2 * s.r), (int) (2 * s.r), s.rot,
					Math.PI + s.rot, s.color.getRGB());
	}

	public void drawSlime(Graphics2D g, Slime s, boolean b) {

		g.setStroke(new BasicStroke(4));

		if (Math.abs(s.rot) <= .01) {
			g.setColor(s.color);
			g.fillArc((int) (s.x - s.r), (int) ((getHeight() - 50 - s.y) - s.r), (int) (2 * s.r), (int) (2 * s.r), 0,
					180);
			g.setColor(s.borderColor);
			g.drawArc((int) (s.x - s.r), (int) ((getHeight() - 50 - s.y) - s.r), (int) (2 * s.r), (int) (2 * s.r), 0,
					180);
		} else {
			g.setColor(s.color);
			g.fillArc((int) s.x - (int) (s.r), getHeight() - 50 - (int) s.y - (int) (s.r), (int) (2 * s.r),
					(int) (2 * s.r), (int) (180 * s.rot / Math.PI), 180);
			g.setColor(s.borderColor);
			g.drawArc((int) s.x - (int) (s.r), getHeight() - 50 - (int) s.y - (int) (s.r), (int) (2 * s.r),
					(int) (2 * s.r), (int) (180 * s.rot / Math.PI), 180);

		}

		g.setStroke(new BasicStroke(1));

		if (drawV && b) {
			g.setColor(Color.white);
			g.setStroke(new BasicStroke(3));
			g.drawLine((int) s.x, (int) (getHeight() - 50 - s.y), (int) s.x + (int) (3 * s.vx),
					(int) (getHeight() - 50 - s.y) - (int) (3 * s.vy));
			g.drawLine((int) s.x + (int) (2 * s.vx - s.vy), (int) (getHeight() - 50 - s.y) - (int) (2 * s.vy + s.vx),
					(int) s.x + (int) (3 * s.vx), (int) (getHeight() - 50 - s.y) - (int) (3 * s.vy));
			g.drawLine((int) s.x + (int) (2 * s.vx + s.vy), (int) (getHeight() - 50 - s.y) - (int) (2 * s.vy - s.vx),
					(int) s.x + (int) (3 * s.vx), (int) (getHeight() - 50 - s.y) - (int) (3 * s.vy));
			g.setStroke(new BasicStroke(1));
		}
	}

	@Override
	public void keyReleased(KeyEvent evt) {
		if (mode == 3) {
			reset();
			play();
		} else if (mode == 4) {
			menu.setVisible(true);
			mode = 0;
		}
		if (KeyEvent.VK_P == evt.getKeyCode()) {
			mode = mode == 2 ? 1 : 2;
		}

		down.remove(evt.getKeyCode());
	}

	@Override
	public void keyTyped(KeyEvent evt) {
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		down.add(evt.getKeyCode());
	}

	public void updateRot(Slime... slimes) {
		for (Slime s : slimes) {
			s.rot += s.vr;
			s.vr *= .95;

			if (isResting(s)) {
				s.vr = s.rot < Math.PI / 2 ? s.vr - s.rot / (50 * Math.PI) : s.rot < 3 * Math.PI / 2 ? s.vr * .8 : s.vr
						+ (2 * Math.PI - s.rot) / (50 * Math.PI);
			}
		}
	}

	public boolean isAngleBetweenCounterClockwise(double angle, double b1, double b2) {
		return 0 <= validateAngle(angle - b1) && validateAngle(angle - b1) <= validateAngle(b2 - b1);
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (mode == 1) {
				netHeight += (netHeightTo - netHeight) / 5;
				hwall += (hWallTo - hwall) / 5;

				player1.move(down, this, powerups);
				player2.move(down, this, powerups);

				updateRot(player1.slime, player2.slime);

				update(player1);
				update(player2);

				lock.lock();
				if (!toAdd.isEmpty() && Math.random() > .8)
					powerups.add(toAdd.poll());
				while (!toRem.isEmpty())
					powerups.remove(toRem.poll());

				for (Powerup p : powerups)
					p.y += GRAVITY;

				while (!showerAdd.isEmpty())
					showers.add(showerAdd.poll());
				for (int i = 0; i < showers.size(); i++)
					if (showers.get(i).isDone())
						showers.remove(i);

				for (ParticleShower p : showers)
					p.update(GRAVITY);
				lock.unlock();

				if (player1.score >= maxScore || player2.score >= maxScore) {
					mode = 4;
					message = "Game Over- " + (player1.score > player2.score ? player1.name : player2.name) + " won!";
				}

				updateBall();
				requestFocus();

			}
			if (mode == 2) {
				pauseRoation += .04;
				requestFocus();
			}
			paint();
			repaint();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (mode == 2)
			play();
		if (mode == 4)
			restartGame();

	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public static void main(String[] args) {
		Main.main(args);
	}
}
