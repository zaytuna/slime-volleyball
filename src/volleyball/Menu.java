package volleyball;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

public class Menu extends JPanel implements ActionListener {
	private static final long serialVersionUID = 7693018989681578913L;

	String[] playerOpt = { "Human (WASD EQ)", "Human (Arrow keys ./)", "Human (IJKL UO)", "AI (Easy)" };

	final Slime[] presets = {
			// RADIUS, SPEED, AGILITY, COLOR
			new Slime(80, 6, .04, Color.ORANGE), new Slime(40, 12, .04, Color.CYAN),
			new Slime(100, 3, .04, Color.GREEN), new Slime(60, 6, .1, Color.RED),
			new Slime(80, 4, .08, Color.LIGHT_GRAY), new Slime(80, 6, .1, Color.DARK_GRAY),
			new Slime(50, 2, .04, Color.ORANGE), // Teleports
			new Slime(40, 3, .04, Color.PINK), // Has Telekenesis
			new Slime(30, 4, 0, Color.BLUE), new Slime(30, 10, 1, new Color(0, 0, 100)) }; // Autodirect

	/**
	 * Powers slimes can have: Teleportation? Telekenisis? Gun? Super Spike- aim
	 * bot.
	 */

	JPanel slimes = new JPanel(new FlowLayout(FlowLayout.CENTER));
	DrawSlimeButton slime1 = new DrawSlimeButton(presets[(int) (Math.random() * 10)]), slime2 = new DrawSlimeButton(
			presets[(int) (Math.random() * 10)]);

	ChooseSlimePanel choose = new ChooseSlimePanel();
	JComboBox<String> p1 = new JComboBox<String>(playerOpt), p2 = new JComboBox<String>(playerOpt);

	JSlider gravity = new JSlider(JSlider.HORIZONTAL, 1, 200, 60), maxPowerups = new JSlider(JSlider.HORIZONTAL, 0, 50,
			10);
	JLabel gravLabel = new JLabel("Gravity (from .1 to 20 pix/frame^2)"), powerLabel = new JLabel("Powerup frequency");

	JTextField name1 = new JTextField(20), name2 = new JTextField(20);
	JSpinner maxScore = new JSpinner();

	JButton okay = new JButton("Done");

	GamePanel owner;

	Menu(GamePanel gw) {
		setLayout(new BorderLayout());

		this.owner = gw;

		p2.setSelectedIndex(1);
		p1.setSelectedIndex(0);

		name1.setPreferredSize(new Dimension(120, 35));
		name2.setPreferredSize(new Dimension(120, 35));

		Box center = new Box(BoxLayout.Y_AXIS);

		JPanel plrs = new JPanel(new FlowLayout(FlowLayout.CENTER));
		plrs.setOpaque(false);
		plrs.add(p1);
		plrs.add(new JLabel(" VS "));
		plrs.add(p2);

		JPanel names = new JPanel(new FlowLayout(FlowLayout.CENTER));
		names.setOpaque(false);
		names.add(new JLabel("Player 1's name: "));
		names.add(name1);
		names.add(new JLabel("Player 2's name: "));
		names.add(name2);

		JScrollPane pane = new JScrollPane(slimes);
		pane.setBorder(new EmptyBorder(0, 0, 0, 0));
		// slimes.setBackground(Color.BLACK);
		// pane.setOpaque(false);
		slimes.add(slime1);
		slimes.add(slime2);

		JPanel powF = new JPanel(new FlowLayout(FlowLayout.CENTER));
		powF.setOpaque(false);
		powF.add(powerLabel);
		powF.add(maxPowerups);
		maxPowerups.setOpaque(false);

		JPanel grav = new JPanel(new FlowLayout(FlowLayout.CENTER));
		grav.setOpaque(false);
		grav.add(gravLabel);
		grav.add(gravity);
		gravity.setOpaque(false);

		JPanel scoring = new JPanel(new FlowLayout(FlowLayout.CENTER));
		scoring.setOpaque(false);
		scoring.add(new JLabel("Max score:"));
		scoring.add(maxScore);
		maxScore.setPreferredSize(new Dimension(100, 25));
		maxScore.setValue(8);

		center.add(plrs);
		center.add(names);
		center.add(pane);
		center.add(grav);
		center.add(powF);
		center.add(scoring);
		JPanel temp = new JPanel(new FlowLayout(FlowLayout.CENTER));
		temp.setOpaque(false);
		temp.add(okay);
		center.add(temp);

		// center.setBackground(Color.BLACK);

		add(center, BorderLayout.CENTER);

		slime1.addActionListener(this);
		okay.addActionListener(this);
		slime2.addActionListener(this);

		add(choose, BorderLayout.EAST);
		choose.setVisible(false);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == slime1 || evt.getSource() == slime2) {
			choose.setVisible(((JToggleButton) evt.getSource()).isSelected());
		} else if (evt.getSource() == okay) {
			switch (p1.getSelectedIndex()) {
			case 0:
				owner.player1 = new HumanPlayer(1);
				break;
			case 1:
				owner.player1 = new HumanPlayer(0);
				break;
			case 2:
				owner.player1 = new HumanPlayer(2);
				break;
			default:
				owner.player1 = new VolleyballAI();
				break;
			}

			switch (p2.getSelectedIndex()) {
			case 0:
				owner.player2 = new HumanPlayer(1);
				break;
			case 1:
				owner.player2 = new HumanPlayer(0);
				break;
			case 2:
				owner.player2 = new HumanPlayer(2);
				break;
			default:
				owner.player2 = new VolleyballAI();
				break;
			}

			owner.player1.slime = slime1.s.clone();
			owner.player2.slime = slime2.s.clone();

			owner.player1.name = name1.getText();
			owner.player2.name = name2.getText();

			GamePanel.GRAVITY = (double) gravity.getValue() / 100D;
			owner.maxPowerups = maxPowerups.getValue();
			owner.maxScore = (Integer) maxScore.getValue();

			owner.setGame();
			owner.reset();

			owner.play();
		}

		for (int i = 0; i < presets.length; i++) {
			if (evt.getSource() == choose.buttons[i]) {
				if (slime1.isSelected())
					slime1.setSlime(presets[i]);
				else
					slime2.setSlime(presets[i]);

				slime1.setSelected(false);
				slime2.setSelected(false);
				choose.setVisible(false);
				slimes.revalidate();
			}
		}
	}

	private class ChooseSlimePanel extends JPanel {
		private static final long serialVersionUID = 5861051519849818629L;

		JToggleButton[] buttons = new JToggleButton[presets.length];

		ChooseSlimePanel() {
			setLayout(new GridLayout(6, 2));

			ButtonGroup group = new ButtonGroup();

			for (int i = 0; i < presets.length; i++) {
				buttons[i] = new DrawSlimeButton(presets[i]);
				group.add(buttons[i]);
				buttons[i].addActionListener(Menu.this);
				add(buttons[i]);
			}
			add(new JButton("+"));

			setPreferredSize(new Dimension(300, 600));
		}
	}

	private class DrawSlimeButton extends JToggleButton {
		private static final long serialVersionUID = -2677143045836661763L;
		Slime s;

		DrawSlimeButton(Slime s) {
			this.s = s;
			setPreferredSize(new Dimension((int) (3 * s.r), (int) s.r + 10));
			setBackground(Color.BLACK);
		}

		public void setSlime(Slime s) {
			this.s = s;
			setPreferredSize(new Dimension((int) (3 * s.r), (int) s.r + 10));
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(s.color);

			g.fillArc(getWidth() / 2 - (int) (s.r), getHeight() / 2 - (int) (s.r / 2), (int) (2 * s.r),
					(int) (2 * s.r), 0, 180);
		}
	}
}
