package volleyball;

import java.awt.Color;
import java.util.Set;

public class VolleyballAI extends Player {

	VolleyballAI() {
		slime = new Slime();
		slime.color = new Color(255, 205, 152);
	}

	VolleyballAI(double x, double y, double r, double a, Color c) {
		slime = new Slime(x, y, r, a, c);
	}

	@Override
	void move(Set<Integer> keys, GamePanel gp, java.util.List<Powerup> p) {
		if (gp.ballY > slime.r + slime.y && 17 * 17 / (2 * GamePanel.GRAVITY) > gp.ballY
				&& slime.x - slime.r - gp.ballR < gp.ballX + 4 * gp.ballVX
				&& slime.x + slime.r + gp.ballR > gp.ballX + 4 * gp.ballVX)
			slime.jump();
		if (gp.ballY - gp.ballR < slime.y)
			slime.vy -= 5;
		if (gp.ballX + gp.ballVX * (gp.ballY - slime.y - slime.r) / 10 < slime.x
				+ (slime.side ? slime.r / 2 : -slime.r / 2))
			slime.left();
		if (gp.ballX + gp.ballVX * (gp.ballY - slime.y - slime.r) / 10 > slime.x
				+ (slime.side ? slime.r / 2 : -slime.r / 2))
			slime.right();

		if ((gp.ballY - slime.y) * (gp.ballY - slime.y) + (gp.ballX - slime.x) * (gp.ballX - slime.x) < (gp.ballR
				+ slime.r + 20)
				* (gp.ballR + slime.r + 20))
			slime.rotR();
		else
			slime.rot += -slime.rot / 4;

		if (collected.size() > 0)
			super.applyFirst(gp);
	}
}