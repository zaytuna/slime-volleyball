package volleyball;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Main {
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	public static void main(String[] args) {
		JFrame window = new JFrame("Slime Games");
		GamePanel game = new GamePanel(800, 600);
		window.add(game, BorderLayout.CENTER);

		window.pack();
		// window.setUndecorated(true);
		// window.setAlwaysOnTop(true);
		window.setLocation(SCREEN.width / 2 - window.getWidth() / 2, SCREEN.height / 2 - window.getHeight() / 2);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}
}